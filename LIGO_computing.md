# LIGO practical computing doc

Some useful links for computing issues related to the LIGO clusters.

## General Info

An overview of the IGWN computing grid can be found [[here](https://computing.docs.ligo.org/guide/dhtc/)]. 

To submit a job in the cluster, the workload management system used is [[HTCondor](https://computing.docs.ligo.org/guide/htcondor/)]. The introductory [videos](https://www.youtube.com/watch?v=oMAvxsFJaw4&list=PLO7gMRGDPNumCuo3pCdRk23GDLNKFVjHn) provide a good overview of the HTCondor system, while the [manual](https://htcondor.readthedocs.io/en/latest/users-manual/index.html) provides extra explanations.

_LIGO specific commands_

__Job Accounting__ [[here](https://computing.docs.ligo.org/guide/htcondor/accounting/)] is important for LIGO submission jobs. To produce accounting tags depending on your work, follow the details [[here](https://accounting.ligo.org/user)].

__Machine Resources__ [[here](https://computing.docs.ligo.org/guide/htcondor/resources/)] are also mandatory. This means that each script requires:

- `request_disk = ... (MB)`

- `request_memory = ... (MB)`

  

## Uploading/Downloading/Searching Data

Your standard password will be needed.

- To _upload_ to a LIGO cluster, from your local terminal do:

  ``` scp <filename> marios.kalomenopoulos@ldas-grid.ligo.caltech.edu:<directory>```

  In general, `ldas-grid.ligo.caltech.edu` can be changed to any option from [[here](https://computing.docs.ligo.org/guide/computing-centres/ldg/#ligo-ssh-key)], in the `<hostname>` directories.

- To _download_ from a LIGO cluster, navigate to the desired directory with your local terminal and do:
  ``` scp marios.kalomenopoulos@ldas-grid.ligo.caltech.edu:<file_directory> .```

- Within LIGO, the different `home` directories are connected. For example, if a file is created in `/home/marios.kalomenopoulos` in `ldas-pcdev11`, it will also be visible in `ldas-grid`.

- Within LIGO, when given the directory of a file, at a specific host - for example `LDAS@CIT` - you can move the file to your `home` directory at the specific host, using just `scp /home/albert.einstein/<file_directory> .`. As above, the idea is that the `home` directories are connected.

- Having the cluster information, for example `CIT`, you can search at whichever directory within `CIT`, irrespective of specific cluster directory, by just giving the details `/home/albert.einstein/<directory>`.

# HTCondor

## 1) Running a simple python job

A simple example on running a python job in HTCondor can be found [[here](https://hcc.unl.edu/docs/osg/a_simple_example_of_submitting_an_htcondor_job/)]. 

__Note__: Some changes are needed in the `hello.submit` file, due to LIGO specific configuration. Check `/home/marios.kalomenopoulos/projects/Condor_test/condor_hello.submit` in `CIT(Production)/ldas-grid`.

Other useful links for `.submit` templates for different applications can be found: 

1. [[link](https://research.iac.es/sieinvens/siepedia/pmwiki.php?n=HOWTOs.CondorSubmitFile)]: Comments on basic `.submit` commands, templates on working with input/output files, external scripts, loops etc
2. [[link](https://research.cs.wisc.edu/htcondor/tutorials/intl-grid-school-3/submit_first.html)]: Simple `.submit` script and example with parameter sweep

__Note 1__: Make sure all permissions are given to the python scripts: `chmod +rwx` (to read, write and execute)

__Note 2__: Python files need a [shebang](https://en.wikipedia.org/wiki/Shebang_%28Unix%29) on top in order to run $\rightarrow$ this helps identify an interpreter. For example: `#!/usr/bin/env python3.6` or if you want to use a specific environment: `#!/home/marios.kalomenopoulos/.conda/envs/autoregressive-bbh-inference/bin/python`



## 2) Running a simple python in a specific environment

LIGO info on Conda environments can be found [[here](https://computing.docs.ligo.org/guide/htcondor/conda/)].

For a submit file, check `/home/marios.kalomenopoulos/projects/Condor_test/condor_env_hello.submit` in `CIT(Production)/ldas-grid`.

__Note__: A [shebang](https://en.wikipedia.org/wiki/Shebang_%28Unix%29) is also needed in this case at the top of the python file, but for the specific environment. For example: `#!/home/marios.kalomenopoulos/.conda/envs/autoregressive-bbh-inference/bin/python`



## 3) Running a Jupyter notebook

To run a Jupyter notebook in the cluster (this happens only in the head nodes - not straightforward to run in a node), click: https://jupyter.ligo.caltech.edu (`url` depends on the _cluster_).

__Running a Jupyter Notebook in a specific environment__

In summary, we need to install the jupyter lab functionality for our specific environment. The steps to do this are the following:

1. Suppose your conda environment is `autoregressive-bbh-inference`.
2. You can install a specific kernel (i.e. jupyter lab functionality) that runs with that, using:

```
conda install -c conda-forge ipykernel
python -m ipykernel install --user --name autoregressive-bbh-inference --display-name "(bbh infer)"
```
(it is probably good, but potentially not necessary, to have activated the environment first using `conda activate autoregressive-bbh-inference`)

1. You open the LIGO jupyter lab as before (click [[here](https://jupyter.ligo.caltech.edu)]) and then you select the kernel of your environment (as in the picture below):

<img src="LIGO_jl_environment.png" alt="image" style="zoom:40%;" />

2. You can also access JupyterLab servers on all other LDG clusters, see their respective URLs [linked from here](https://computing.docs.ligo.org/guide/computing-centres/ldg/). Note that each cluster is independent from each other (ie. environments you've set up at one LDG site is not automatically avaliable elsewhere). Different head nodes and JupyterLab servers at one LDG site share file system though, so they will have access to the same environments and data [**MK: CHECK THIS**].



## 4) Running python jobs in parallel

(_Under development_) - Link to check: [here](https://condor.liv.ac.uk/python/).



# JupyterLab in LDG

To login to a specific host, and be able to open a `jupyter lab` in a specific environment:

```
ssh marios.kalomenopoulos@ldas-pcdev11.ligo.caltech.edu -L8881:localhost:8881
conda activate environment_name
jupyter lab --no-browser --port=8881
```





## A) Useful commands in HTCondor

- Basic commands:

  1. To submit a job: `condor_submit <file_name>.sub`
  2. To submit a dag (directed acyclic graph, a collection of jobs): `condor_submit_dag <file_name>.dag`
  3. To check running jobs: `condor_q` - for a nicer, and more informative list, you can also add some flags and run the command `condor_q albert.einstein -nobatch -dag`. If `-submitter albert.einstein` is also included, you get information about jobs running in all of the clusters.
  4. To delete a job: `condor_rm <job_id>`
     

- Command for some useful info on __idle__ (or __held__) jobs:

  `condor_q <jobID> -analyze`, or for a better analysis `condor_q <jobID> -better-analyze`

- It is possible to change parameters in the __submit__ file for __idle__ jobs. For example, change the requested memory:

  `condor_qedit <jobID> RequestMemory=1000` (memory in MB)

- For __held__ jobs, there are similar options:

  1. To _identify_ use: `condor_q -held` 
  
  2. To _edit_ use: `condor_qedit` -  For example, if your job has run out of memory, you can increase its limit like `condor_qedit <jobID> RequestMemory=[new value in MB]`
  
  3. To _release_ use: `condor_release <jobID>` (you can also release many jobs at once `condor_release <jobID_1> <jobID_2> <jobID_3>`), or to release all your held jobs: `condor_release albert.einstein`

  4. List of info about a job can be found [here](https://web.ma.utexas.edu/condor/manual/2_6Managing_Job.html).

# Misc

- To add aliases, go to `/home` directory and modify `.bashrc`. For example:

  ```
  alias ll="ls -lh"
  ```

  After defining any new changes, for these to work, we either need to close/re-open the terminal, or "source" the changes with:

  ```
  source ~/.bashrc *or* . ~/.bashrc
  ```

  
